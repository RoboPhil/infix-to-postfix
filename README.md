## Read me ##
Reference the js script to your page html page, 
pass the arithmetic problem you wanna solve and voila it returns an ans,
oh.. if there's an error, it returns false.

##Example.##
```sh
var expression = "2+3*4-5/6+(7*8-9/10)";
var ans = infix2postfix(inputStr);

if(!ans){
    console.log("invalid expression passed");
}else{
    $("#ans").text(ans);
    console.log("ans to "+expression+" is = "+ans);
}
```